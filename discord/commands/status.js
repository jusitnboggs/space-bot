const ServerQuery = require ('game-server-query');
const Discord = require('discord.js');
const Logger = require('winston');
const Config = require('../../config.json');

function DoServerQuery(ServerData, OutputChannel)
{
	ServerQuery(
		// Query Data
		{
			type: ServerData.Type,
			host: ServerData.ServerIP,
			port: ServerData.Port,
			port_query: ServerData.QueryPort
		},

		function (QueryState)
		{
			if (!QueryState.error)
			{
				var Output = `${QueryState.name}`;

				if (QueryState.map !== undefined && QueryState.map !== '')
				{
					Output += ` - ${QueryState.map}`;
				}

				if (ServerData.UseConnectURL)
				{
					Output += `\nJoin Here: ${ServerData.ConnectURLProtocol}${ServerData.ServerIP}:${ServerData.Port}`;
				}
				else
				{
					Output += `\nJoin Here: ${ServerData.ServerIP}:${ServerData.Port}`;
				}

				Output += `\nPlayers: (${QueryState.players.length}/${QueryState.maxplayers})`;

				if (QueryState.players.length > 0)
				{
					Output += `: `;

                    var bIsFirst = true;
					for (const Player of QueryState.players)
					{
                        // Make sure the player has a name, and if that it isn't client type 1 (teamspeak 3-specific)
						if (Player.name !== undefined && Player.name !== '' && Player.client_type != 1)
						{
                            if (bIsFirst)
                            {
                                Output += `${Player.name}`;
                            }
                            else
                            {
                                Output += `, ${Player.name}`;
                            }
                            bIsFirst = false;
						}
					}
				}

				OutputChannel.send(Output);
			}
			else
			{
				Logger.error(QueryState.error);
			}
		}
	);
}

module.exports = {
	Name: 'status',
	Description: 'Get the status of the server',
	AdminOnly: false,
	Execute(Message, Args) {
		// If they didn't specify, send a basic status of all the servers
		if (Args.length <= 0)
		{
			for (const ServerData of Object.values(Config.ServerInfo))
			{
				DoServerQuery(ServerData, Message.channel);
			}
		}
		else
		{
			const ServerName = Args.join(' ');
			const ServerData = Config.ServerInfo[ServerName];

			if (ServerData !== undefined)
			{
				DoServerQuery(ServerData, Message.channel);
			}
			else
			{
				Message.channel.send(`There is no server with the name "${ServerName}"`)
			}
		}
	},
};