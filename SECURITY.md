# Security Policy

## What is a Vulnerability

A vulnerability is an exploit in the software which can be used to gain unauthorized access to resources that are not granted to users via normal means, or create lasting and permanent damage on the host machine.

Please note that crashes are not considered a vulnerability that needs to be reported through this process. Please report them using the standard issue reporting process.

## Supported Versions

The Space Combat 2 Team only supports the gitlab master branch, and the latest release tag. Versions other than this are unsupported.

## Reporting a Vulnerability

You can report a vulnerability to us by creating a new issue [here](https://gitlab.com/TeamDiaspora/space-bot/issues/new) and marking it as confidential. Please include `[SECURITY]` at the front of your title, like so `[SECURITY] Vulnerability allows user to download all duplications`.

**Please make sure to mark your issue as confidential, or it will be visible to anyone!**

## How the Space Combat 2 Team will handle a Vulnerability

Space Combat 2 development will keep the details of a vulnerability private until fixes are fully deployed. At that time, we will send out a notification on discord urging users and server operators to update.
